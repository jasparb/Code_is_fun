#!/usr/bin/env python
#
# Code Philippe  Di Stefano, Queen's
# 2017 10 10
# This code just reads a text file, and tries to replace certain words
# Students should fully document this code

import random

verbose = True

class swap(object):
	def __init__(self, fileName="StCrispinsDay.txt", path="/Users/distefano/Documents/Code_Svn/PHYS_313/"):
		self.fullInFileName= path+"/"+fileName
		tmp=self.fullInFileName.split(".")
		self.fullOutFileName=tmp[0] + "_modified" + ".txt"
		if verbose:
			print "Original file: " + self.fullInFileName
			print "Modified file: " + self.fullOutFileName
			
		outputFile = open(self.fullOutFileName, "w")
		with open(self.fullInFileName, 'r') as inputFile:
			for self.line in inputFile:
				self.doSomethingToLine()
				outputFile.write(self.line)
		outputFile.close()

	def doSomethingToLine(self):
		self.olds=["brother", "man", "men"]
		self.news=["bro", "dude", "peeps"]

		for old, new in zip(self.olds, self.news):
			if  old in self.line:
				if verbose:
					print "Caught \"" + old + "\" in line \"" + self.line.strip() + "\",",
					print "replacing with \"" +new + "\""
				self.line=self.line.replace(old, new)

	
class swapRandom(swap):
	def __init__(self, fileName="Tune.txt", path="/Users/distefano/Documents/Code_Svn/PHYS_313/"):
		swap.__init__(self, fileName, path)

	def doSomethingToLine(self):
		self.olds=["feels"]
		self.news=["peels","eels","meals","keels","wheels","deals", "heels", "steels", "steals", "reels", "yields"]

		for old in self.olds:
			if  old in self.line:
				new=self.news[random.randint(0,len(self.news)-1)]
				if verbose:
					print "Caught \"" + old + "\" in line \"" + self.line.strip() + "\",",
					print "replacing with \"" +new + "\""
				self.line=self.line.replace(old, new)
		
if __name__ == '__main__':
	path="/Users/distefano/Documents/Code_Svn/PHYS_313/"
	sw=swap(path=path)
	swr=swapRandom(path=path)